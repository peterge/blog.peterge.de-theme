# blog.peterge.de-theme

Live Version of the Theme and more available under https://blog.peterge.de/!

![image](https://gitlab.com/peterge/blog.peterge.de-theme/-/raw/main/images/initial-layout.png)

# Source

My own version of this theme is forked from [this](https://github.com/mityalebedev/The-Shell) Ghost theme.

# Changelog

## 2023-04-24

- Fixed ``<title>``
- Updated the sidebar to include recommended posts

![image](https://gitlab.com/peterge/blog.peterge.de-theme/-/raw/main/images/updated-sidebar.png)

## 2023-04-25

- Update index.hbs & post.hbs to include the [``partial/sidebar.hbs`` partial file](https://ghost.org/docs/themes/helpers/partials/) via ``{{> "sidebar"}}`` instead of having to update both files, each time I update the list.
